FROM frolvlad/alpine-oraclejdk8

ADD project.clj /app/
ADD src/z048/core.clj /app/src/z048/core.clj

ENV LEIN_ROOT 1
RUN apk add --update bash openssl \
 && wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein \
    -O /usr/local/bin/lein \
 && chmod 0755 /usr/local/bin/lein \
 && lein upgrade \
 && apk del openssl \
 && cd /app \
 && lein uberjar \
 && rm /app/target/uberjar+uberjar/z048-0.1.0-SNAPSHOT.jar

CMD java -Xmx3584m -jar /app/target/uberjar/z048-0.1.0-SNAPSHOT-standalone.jar

