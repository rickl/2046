# 2046: Yet Another 2048 Solver

A 2048 solver written in Clojure.

## Build

See Dockerfile, or just run `lein uberjar` if you already have Leiningen installed.

## Run

See Dockerfile, or just run

    $ java -Xmx3584m -jar z048-0.1.0-standalone.jar [args]

Solving steps are printed to stdout, while debug logs to stderr.

## Args

`-q` : quiet mode, print only the final result.

### Bugs

...

## License

Copyright © 2015 Rick Lei

Distributed under the Apache License 2.0.

