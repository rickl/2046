(ns z048.core
  (:use [clojure.string :only (join)])
  (:gen-class))

(declare game-2048-aa)

(defn -main
  [& [arg]]
  (game-2048-aa (not= arg "-q")))

(def EMPTY_VALUE 128)

(def BOARD_SIZE 4)

(def LOSE -1000000)

(defn ^int evaluate-pair [[^int a ^int b]]
  ;(cond (= a b) (let [value (if (= a 0) EMPTY_VALUE a)]
  (cond (= a 0) EMPTY_VALUE
        (= a b) (+ (int (Math/pow a 1.2)) (/ a 2))
        (> a b) (+ (int (Math/pow a 1.2)) (if (= (/ a 2) b) (/ a 4)
                                            0)) ; (+ a (/ b 2)))
        ;(= a (/ b 2)) (/ a 2)
        :else (- (int (Math/pow a 1.2))
                 (let [bb (int (Math/pow b 1.2))]
                   (cond ;(<= b 32) b
                         (<= b 64) (+ bb bb)
                         (<= b 512) (* bb 4)
                         :else (* bb 6))))))

(defn iter-row-pair [xs] (partition 2 1 (concat xs [0])))

(defn ^int evaluate-row [xs]
  (reduce + (map evaluate-pair (iter-row-pair xs))))

(defn mk-board [xs] (partition BOARD_SIZE xs))
(defn rows [xs] xs)
(defn columns [xs] (apply map vector (rows xs)))

(def row-mask (dec (bit-shift-left 1 (* 4 BOARD_SIZE))))

(def log-base-2 (into (into {} (map #(hash-map (bit-shift-left 1 %) %) (range 1 16))) {0 0}))

(def power-of-2 #(if (zero? %) 0 (bit-shift-left 1 %)))

(defn unpack-board [compact-board]
  (map #(bit-and row-mask (bit-shift-right compact-board %)) (range (* 4 (dec BOARD_SIZE) BOARD_SIZE) -1 (* -4 BOARD_SIZE))))

(defn unpack-row [compact-row]
  (map #(power-of-2 (bit-and 0xf (bit-shift-right compact-row %))) (range (* 4 (dec BOARD_SIZE)) -1 -4)))

(defn transpose-compact-rows [compact-rows]
  (for [shift-bits (range (* 4 (dec BOARD_SIZE)) -1 -4)]
    (reduce #(+ (bit-shift-left %1 4) %2) 0
            (map #(bit-and 0xf (bit-shift-right % shift-bits)) compact-rows))))

(defn pack-row [xs]
  (reduce #(+ (bit-shift-left %1 4) (log-base-2 %2)) 0 xs))

(defn pack-board [xs]
  (reduce #(+ (bit-shift-left %1 (* 4 BOARD_SIZE)) %2) 0 xs))

(def evaluate-row-cached (memoize evaluate-row))

(def row-evaluate evaluate-row-cached)

(defn evaluate-compact-row [row] (evaluate-row (unpack-row row)))

(def evaluate-compact-row-cached (memoize evaluate-compact-row))

(declare simple-move)
(declare mk-compact-board)

(defprotocol Board
  "Game board"
  (get-rows [this] "get all rows")
  (evaluate [this] "evaluate board score")
  (empty-cells [this] "get list of offsets of all empty cells")
  (move [this dir] "move to direction")
  (place-piece [this pos number] "place a piece"))

(defn evaluate-compact-rows-cols [rows cols]
  (+ (reduce + (map * [2 1 1 1] (map evaluate-compact-row-cached rows)))
     (reduce + (map * [2 1 1 1] (map evaluate-compact-row-cached cols)))))

(defn primary-row [row col]
  (if (>= (bit-shift-right row (* 4 (dec BOARD_SIZE))) 9)  ; >= 512
    (if (> row col) :row :col)
    :no-primary-row))

(defn reverse-compact [row]
  (pack-row (reverse (unpack-row row))))

(def reverse-compact-cached (memoize reverse-compact))

(defn snake-rows [rows]
  ;(into (conj [] (first rows) (reverse-compact-cached (second rows))) (drop 2 rows)))
  (into (conj [] (first rows)) (map reverse-compact-cached (rest rows))))

(defn evaluate-primary-row-focused [rows cols]
  (condp = (primary-row (first rows) (first cols))
    :row (let [snake (snake-rows rows)]
           (+ (reduce + (map * [3 1 1 1] (map evaluate-compact-row-cached snake)))
              (reduce + (map evaluate-compact-row-cached cols))))
    :col (let [snake (snake-rows cols)]
           (+ (reduce + (map * [3 1 1 1] (map evaluate-compact-row-cached snake)))
              (reduce + (map evaluate-compact-row-cached rows))))
    (evaluate-compact-rows-cols rows cols)))

(defrecord CompactBoard [^Long bitmap]
  Board
  ;"Compact board representation using Long as bitmap"
  (get-rows [this] (map unpack-row (unpack-board bitmap)))
  (evaluate [this]
    (let [rows (unpack-board bitmap),
          cols (transpose-compact-rows rows)]
      ;(evaluate-primary-row-focused rows cols)))
      (evaluate-compact-rows-cols rows cols)))
  ;(condp = (primary-row (first rows) (first cols))
  ;  :row (evaluate-compact-rows-cols (snake-rows rows) cols)
  ;  :col (evaluate-compact-rows-cols (snake-rows cols) rows)
  ;  (evaluate-compact-rows-cols rows cols))))
  (empty-cells [this]
    (filter some?
            (for [offset (range (* BOARD_SIZE BOARD_SIZE))]
              (let [shift-bits (* (- (* BOARD_SIZE BOARD_SIZE) offset 1) 4)]
                (if (zero? (bit-and bitmap (bit-shift-left 0xf shift-bits))) offset nil)))))
  (move [this dir] (CompactBoard. (pack-board (map pack-row (simple-move (get-rows this) dir)))))
  (place-piece [this pos number]
    (CompactBoard. (bit-or bitmap (bit-shift-right (bit-shift-left (log-base-2 number) (- (* BOARD_SIZE BOARD_SIZE 4) 4)) (* 4 pos))))))

(defn mk-compact-board [xs]
  (CompactBoard. (pack-board (map pack-row (mk-board xs)))))

(defn num-empty-cells [board] (count (empty-cells board)))

(defn place-2-4 [board]
  (let [n (rand-nth [2 4]),
        insert-pos (rand-nth (empty-cells board))]
    (place-piece board insert-pos n)))

(defn ^int evaluate-board [board]
  (let [rows (get-rows board), cols (columns rows)]
    (+ (reduce + (map row-evaluate rows))
       (reduce + (map row-evaluate cols))
       (condp = (num-empty-cells board)
         4 -2000
         3 -3000
         2 -4500
         1 -7000
         0 -10000
         0)
       ;(evaluate-zeroes xs)
       )))

;(defn ^int evaluate-board-snake [xs]
;  (+ (reduce + (map (fn [[row1 row2]] (+ (row-evaluate row1) (row-evaluate (reverse row2)))) (partition 2 2 (rows xs))))
;     (reduce + (map row-evaluate (columns xs)))
;(evaluate-zeroes xs)
;     ))

;(defn rows-to-board [xs] (flatten xs))
(defn rows-to-board [xs] xs)
(defn columns-to-board [xs] (rows-to-board (apply map vector xs)))

;(defn merge-compact [xs]
;  (loop [xs (filter #(not= 0 %) xs), result []]
;    (if (empty? xs) result
;      (let [[a b & _] xs]
;        (if (= a b) (recur (drop 2 xs) (conj result (+ a b)))
;          (recur (next xs) (conj result a)))))))

(defn merge-compact-oneshot [xs]
  (loop [xs (filter #(not= 0 %) xs), result []]
    (if (empty? xs) result
      (let [[a b & _] xs]
        (if (= a b) (apply conj result (+ a b) (drop 2 xs))
          (recur (next xs) (conj result a)))))))

(defn move-to-head [xs]
  (take BOARD_SIZE (concat (merge-compact-oneshot xs) (repeat 0))))

(defn move-to-tail [xs]
  (reverse (move-to-head (reverse xs))))

(def move-to-head-cached1 (memoize move-to-head))
(def move-to-tail-cached1 (memoize move-to-tail))

(defn simple-move [xs dir]
  (cond
    (= dir :up)    (columns-to-board (map move-to-head-cached1 (columns xs)))
    (= dir :left)  (map move-to-head-cached1 (rows xs))
    (= dir :right) (map move-to-tail-cached1 (rows xs))
    :else          (columns-to-board (map move-to-tail-cached1 (columns xs)))))

(defn next-moves [board]
  (filter #(not= board (val %)) (into {} (map #(hash-map % (move board %)) [:up :left :right :down]))))

(defn obstacles [board]
  (for [pos (empty-cells board), obstacle [2 4]] [pos obstacle]))

(defn worst-obstacle-boards [board]
  (let [flat-board (vec (flatten board))]
    ;(map (fn [[pos obstacle]] (mk-board (assoc flat-board pos obstacle))) (take 9 (shuffle (obstacles flat-board))))))
    (map (fn [[pos obstacle]] (mk-board (assoc flat-board pos obstacle))) (obstacles flat-board))))

(declare alpha-average-obstacle)
(declare alpha-average-obstacle-cached)

(defn ^int alpha-average-player [board depth steps]
  ;(do (println depth a b board max-player)
  (if (zero? depth) (evaluate board)
    (let [next-boards (next-moves board),
          lose (zero? (count next-boards))]
      (if lose (+ LOSE steps)
        (reduce max (map #((if (>= depth 0) alpha-average-obstacle-cached
                             alpha-average-obstacle) (val %) (dec depth) (inc steps)) next-boards))))))

(defn ^int average [xs]
  (int (/ (reduce + xs) (count xs))))

(defn ^int alpha-average-obstacle [board depth steps]
  (if (zero? depth) (evaluate board)
    (let [next-boards (map (fn [[pos n]] (place-piece board pos n)) (obstacles board))]
      (average (map #(alpha-average-player % (dec depth) (inc steps)) next-boards)))))

(def alpha-average-obstacle-cached (memoize alpha-average-obstacle))

;(defn shuffle-init-board [] (mk-board (shuffle (concat [2 2 2] (repeat (- (* BOARD_SIZE BOARD_SIZE) 3) 0)))))
(defn shuffle-init-board [] (mk-compact-board (shuffle (concat [2 2 2] (repeat (- (* BOARD_SIZE BOARD_SIZE) 3) 0)))))

(defn try-search-move [moves init-search-depth last-step-score]
  (loop [search-depth init-search-depth,
         retry 0]
    (let [scored-moves (into {} (pmap #(hash-map % (alpha-average-obstacle (val %) search-depth 1)) moves)),
          best (apply max-key val scored-moves)]
      (if (and (< retry 2) (< (val best) (* last-step-score 3/4)))
        (do
          (.println *err* "extending search")
          (recur (+ search-depth 2) (inc retry)))
        best))))

(defn print-board [board]
  (doall (map #((comp println join) (map (partial format "|%d|") %)) (get-rows board))))

(defn game-2048-aa
  ([] (game-2048-aa true))
  ([log]
   (let [init-board (shuffle-init-board)]
     (if log
       (do
         (println "I")
         (print-board init-board)))
     (loop [board init-board, last-step-score 0]
       (let [moves (next-moves board)]
         (if (empty? moves)
           (do
             (println "E")
             ;(doall (map println (get-rows board)))
             (if (not log) (print-board board))   ; always print final board in quiet mode
             (shutdown-agents))
           (let [num-empty (num-empty-cells board),
                 first-search-depth (cond (> num-empty 8) 4
                                          (> num-empty 3) 5
                                          :else 6),
                 best (try-search-move moves first-search-depth last-step-score),
                 next-board (place-2-4 (val (key best)))]
             (if log
               (do
                 ;(println (key (key best)) (val best))
                 ;(doall (map println (get-rows next-board)))))
                 (println (Character/toUpperCase (second (str (key (key best))))))
                 (.println *err* (str (val best)))
                 (print-board next-board)))
             (recur next-board (val best)))))))))

