(defproject z048 "0.1.0-SNAPSHOT"
  :description "Yet another 2048 resolver"
  :url "https://bitbucket.org/rickl/2046"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]]
                 ;[org.clojure/core.memoize "0.5.8"]]
  :main ^:skip-aot z048.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
